use std::{
	ffi::{OsStr, OsString},
	io::{self, IsTerminal},
	iter::Chain,
	ops::RangeInclusive,
	os::unix::ffi::{OsStrExt, OsStringExt},
	path::PathBuf,
};

mod printer;

struct App;
impl App {
	const HELP: &str = r"
   Description:
      Rename files, keeping unified minimalistic scheme of numbers and letters

   Usage (any of):
      PATHS | renc
              renc PATHS
      PATHS | renc PATHS";
	fn die(self) -> ! {
		std::process::exit(1);
	}
	fn help(self) -> Self {
		eprintln!("{}", Self::HELP);
		self
	}
	fn err(self, msg: impl std::fmt::Display) -> Self {
		eprintln!("[renc] ERROR: {msg}");
		self
	}
	fn end(self) {}
}

type Letters = Chain<Chain<RangeInclusive<u8>, RangeInclusive<u8>>, RangeInclusive<u8>>;

struct Slot {
	letters: Letters,
	state: u8,
}

impl Slot {
	pub fn new() -> Self {
		// 0..=9, A..=Z, a..=z
		let mut letters = (48..=57).chain(65..=90).chain(97..=122);
		Self {
			state: letters.next().unwrap(),
			letters,
		}
	}

	pub fn next(&mut self) -> bool {
		match self.letters.next() {
			Some(letter) => {
				self.state = letter;
				true
			}
			None => {
				*self = Slot::new();
				false
			}
		}
	}
}

struct Line {
	slots: Vec<Slot>,
}

impl Line {
	pub fn new(digits: usize) -> Self {
		let mut slots = Vec::with_capacity(digits);
		(0..digits).for_each(|_| slots.push(Slot::new()));
		Self { slots }
	}

	fn advance_(slots: &mut Vec<Slot>) {
		for slot in slots.iter_mut().rev() {
			if slot.next() {
				return;
			}
		}
		slots.push(Slot::new());
	}

	pub fn advance(&mut self) {
		Self::advance_(&mut self.slots);
	}

	pub fn to_os_string(&self) -> OsString {
		let bytes: Vec<_> = self.slots.iter().map(|c| c.state).collect();
		OsString::from_vec(bytes)
	}
}

fn digits(flen: usize) -> usize {
	let mut dig = 1;
	while 64_usize.pow(dig) < flen {
		dig += 1;
	}
	dig as usize
}

fn main() {
	let mut files: Vec<_> = std::env::args_os().skip(1).map(PathBuf::from).collect();
	if !io::stdin().is_terminal() {
		files.extend(
			std::io::stdin()
				.lines()
				.map_while(Result::ok)
				.map(PathBuf::from),
		);
	}
	if let Some(nonexistent) = files.iter().find(|f| !f.exists()) {
		App.err(format!("file does not exist: {}", nonexistent.display()))
			.end();
	}
	if files.is_empty() {
		App.help().die();
	}

	let mut stdout = io::stdout().lock();
	let printer = printer::get_printer();
	let mut settled = Vec::new();
	let mut line = Line::new(digits(files.len()));

	for old_path in files {
		let ext = if old_path.is_dir() {
			OsStr::new("")
		} else {
			old_path.extension().unwrap_or_default()
		};
		let old_fname = old_path.file_name().unwrap().to_os_string();

		loop {
			let mut new_fname = line.to_os_string();

			if !ext.is_empty() {
				new_fname.push(".");
				new_fname.push(ext);
			}
			let new_path = old_path.with_file_name(&new_fname);

			if settled.contains(&old_fname) {
				break;
			}

			if old_path == new_path {
				line.advance();
				break;
			} else if new_path.exists() {
				settled.push(new_fname);
				line.advance();
			} else {
				std::fs::rename(old_path, &new_path).unwrap();
				line.advance();
				printer
					.println(&mut stdout, new_path.as_os_str().as_bytes())
					.unwrap_or_else(|e| App.err(e).end());
				break;
			}
		}
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_digits() {
		assert_eq!(digits(63_usize), 1);
		assert_eq!(digits(64_usize), 1);
		assert_eq!(digits(65_usize), 2);
	}

	#[test]
	fn test_line_1() {
		let mut line = Line::new(1);
		assert_eq!(line.to_os_string(), OsString::from("0"));
		line.advance();
		assert_eq!(line.to_os_string(), OsString::from("1"));
		(0..35).for_each(|_| line.advance());
		assert_eq!(line.to_os_string(), OsString::from("a"));
	}

	#[test]
	fn test_line_2() {
		let mut line = Line::new(3);
		assert_eq!(line.to_os_string(), OsString::from("000"));
		line.advance();
		assert_eq!(line.to_os_string(), OsString::from("001"));
		(0..61).for_each(|_| line.advance());
		assert_eq!(line.to_os_string(), OsString::from("010"));
	}
}
