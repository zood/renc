use std::io::{self, IsTerminal, Write};

pub trait Print<T: Write> {
	fn println(&self, writer: &mut T, data: &[u8]) -> io::Result<()>;
}

struct PipePrinter;

impl<T: Write> Print<T> for PipePrinter {
	fn println(&self, writer: &mut T, data: &[u8]) -> io::Result<()> {
		writer.write_all(data)?;
		writer.write_all(b"\n")?;
		Ok(())
	}
}

struct DummyPrinter;

impl<T: Write> Print<T> for DummyPrinter {
	fn println(&self, _writer: &mut T, _data: &[u8]) -> io::Result<()> {
		Ok(())
	}
}

pub fn get_printer<T: Write>() -> &'static dyn Print<T> {
	if !io::stdout().is_terminal() {
		&PipePrinter
	} else {
		&DummyPrinter
	}
}
